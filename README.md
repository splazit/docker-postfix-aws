# docker-postfix-aws

A simple Postfix relay container to AWS SES. This should be used in within docker networks such as swarm ingress network etc...

# Prerequisite:
- A domain setup with AWS Simple Email Service
- An identity account associated with AWS SES

# Available docker environment variables:
- AWS_SES_USERNAME (required): Username of the identity account
- AWS_SES_PASSWORD (required): Password of the identify account
- AWS_SES_HOST (optional): The server name of AWS SES. The default is: email-smtp.us-west-2.amazonaws.com
- AWS_SES_PORT (optional): Port of the above server. It could be 25, 465 or 587 The default is 587
- MY_NETWORKS (optional): which network(s) allowed to connect to this postfix. The default is all private networks: 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8
- POSTFIX_HOST (optional): name of this postfix host. The default is localhost

# Example:
docker run -d --rm --name postfix-aws -e AWS_SES_USERNAME=SES_USERNAME -e AWS_SES_PASSWORD=SES_PASSWORD -e POSTFIX_HOST=internal_postfix splazit/postfix-aws